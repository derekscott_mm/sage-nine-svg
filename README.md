```bash

         _______                     _______ __               _______ ___ ___ _______ 
        |     __|.---.-.-----.-----.|    |  |__|.-----.-----.|     __|   |   |     __|
        |__     ||  _  |  _  |  -__||       |  ||     |  -__||__     |   |   |    |  |
        |_______||___._|___  |_____||__|____|__||__|__|_____||_______|\_____/|_______|
        ===============|_____|========================================================                                                   
                       
```

# Sage SVG

[Sage 9](https://roots.io/sage) comes standard with a version of Laravel's Blade templates.

If you have never used Blade templates, you're in for a treat. They bring a much needed improvement to Wordpress theme development.

This package builds onto your existing **Sage 9** install by adding support for SVG inlining. While it's not the only package that does so, it does appear to be the only package that does not break **Webpack 3+** functionality that also comes standard with **Sage 9**.

Namely, using the SVGO loader, it by default is looking for SVGs that have been placed in the `resources/assets/images` directory. Where as other Sage SVG packages have you move your SVGs into `resources/assets/icons` or require you to deeply nest them in the images directory.

Instead this package leaves all the configuring up to you! **Best of all** that configuring is simply prefixing your filename with a single directory name, ie: **@svg('images/filename')** or **@svg('icons/iconfile')** instead of being locked into one folder and out of webpack.


## Installation

You can install this package via Composer by running this command in the root of your Sage 9 installation.

```bash
composer require derekscott_mm/sage-nine-svg
```

## Getting Started

Once installed, the package will automatically register a blade directive to inline your SVGs.

```php

<?php
 // Example:
 //  @param 'Pumpkin_Logo' is the filename, 'images' is the directory
 //  @param 'brand-logo' is the class that will get applied to the SVG.
 ?>

<a class="nav-item" href="/">
  @svg('images/Pumpkin_Logo', 'brand-logo')
</a>

<!-- Renders -->
<a href="/" class="nav-item">
    <svg class="icon brand-logo" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
        <!-- |pumpkin svg code| -->
    </svg>
</a>
```

In this example, we are loading an SVG icon.

```php

<?php
 // Example:
 //  @param 'Horseman' is the filename, 'icons' is the directory
 //  @param 'tiny-icon' is the class that will get applied to the SVG.
 ?>
 
<a class="nav-item" href="/">
  @svg('icons/Horseman', 'tiny-icon')
</a>

```

### Standing on the backs of giants

This repository is a wrapper for Adam Wathan's [Blade SVG](https://github.com/adamwathan/blade-svg) for Laravel, so a 
lot of the concepts from that repository are the same.

If you would like to add in more functionality, or expand on this package, [Adam's](https://github.com/adamwathan/blade-svg) repository is going to be your number one asset.

The only thing this libary does not support at the moment is non-inline SVG. But this is 
[better](https://css-tricks.com/pretty-good-svg-icon-system/) anyway right?


### Credits

Most of the work of this repository is down to Adam Wathan. This is just a wrapper to get it work with Sage 9 blade 
templates.

